const mysql = require('mysql');
const schema = require('./pattern/hostSet.json');
const insertDB = require('./sqlLibs/schemaInsert.js');

insertDB.initSchema(schema);

const express = require('express');
const bodyParser = require('body-parser');
const exphbs = require('express-handlebars');

const host = "0.0.0.0";
const port = 8082;
const app = express();

app.use(express.static('frontend'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.listen(port, host);

app.post('/insert/spio', function(req, res){
  // for(let s in req.body) {
  //   console.dir(req.body[s])
  // }
  let pList = spioParser.parse(req.body);
  rdb.insertSpio(pList);
  res.send(req.body);
});


app.post('/insert/table', function(req, res){
	let nTable = req.body;
	res.send(insertDB.insert(nTable));
});

// handlebars magic here

const hbs = exphbs.create({
  helpers: {
    foo: function() {return 'FOO!!1!!1!!'},
    bar: function() {return 'HOLYBARTENDA'},
  }
});

app.engine('handlebars', hbs.engine);
app.set('view engine', 'handlebars');

app.get('/view/tables/', async (req, res, next) => {
  // let config = {};
  // config.Gala = req.query.gala||0;
  
  let cSpio = await rdb.showSpioList(config);
  res.render('viewTables', {
    spioList: cSpio,

    helpers: {
      fooz: () => { return 'foobazz';}
    }
  });
});


console.log(`Running on http://${host}:${port}`);

module.exports = app;