#!/bin/bash

echo "Start mysql like database in local docker container with local persistent"
mkdir -p MY-DATA/
chmod 777 MY-DATA/ -R
docker run -d -v "$PWD/MY-DATA:/var/lib/mysql" -e "MYSQL_ROOT_PASSWORD=docker" -e "MYSQL_DATABASE=schema"  -p "3206:3306" percona

echo "start is done listen to port 3206"
echo "login with =>"
echo "mysql -uroot -pdocker -h127.0.0.1 -P3206"
