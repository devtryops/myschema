const mysql = require('mysql');
const schema = require('./pattern/hostSet.json');

const rdb = {};
const db_config = {};
db_config.host = (process.env.MYSQL_HOST)? process.env.MYSQL_HOST : 'localhost';
db_config.user = (process.env.MYSQL_USER)? process.env.MYSQL_USER : 'root';
db_config.password = (process.env.MYSQL_PASSWORD)? process.env.MYSQL_PASSWORD : 'docker';
db_config.database = (process.env.MYSQL_DATABASE)? process.env.MYSQL_DATABASE : 'schema';
db_config.port = (process.env.MYSQL_PORT)? process.env.MYSQL_PORT : '3206';
db_config.connectionLimit = 10;

rdb.pool = mysql.createPool(db_config);
rdb.poolQ = (sqlQ,args) => {
  return new Promise( ( resolve, reject ) => {
    rdb.pool.getConnection( (err, conn) => {
      if(err) {
        return reject(err);
      } else if(conn) {
        conn.query(sqlQ, args, (err, rows) => {
          conn.destroy();
          if(err) {
            return reject(err);
          } else if(rows){
            return resolve(rows);
          } else {
            return reject({err,rows});
          }
        });
      }
    });
  });
};

rdb.insertTest = async (spioList) => {
  for(let s in spioList) {
    if((spioList[s].Flotten===undefined||false) || (spioList[s].Verteidigung===undefined||false)) continue;
    let testQuery = `SELECT id FROM berichte WHERE \`Gala\`=${spioList[s].Gala} AND \`System\`=${spioList[s].System} AND \`Planet\`=${spioList[s].Planet} AND \`Time\`=${spioList[s].Time}`;
    let searchProof = await rdb.poolQ(testQuery);
    if(searchProof[0]||false) {
      continue;
    } else {          
      let sqlQuery = 'INSERT INTO berichte (`Gala`,`System`,`Planet`,`Metall`,`Kristall`,`Deuterium`,`Flotten`,`Verteidigung`,`Time`,`Player`,`Planetname`) ';
      sqlQuery += `VALUES (${spioList[s].Gala},${spioList[s].System},${spioList[s].Planet},${spioList[s].Metall},${spioList[s].Kristall},${spioList[s].Deuterium},${spioList[s].Flotten},${spioList[s].Verteidigung},${spioList[s].Time},"${spioList[s].Player}","${spioList[s].Planetname}");`;
      console.log(sqlQuery)
      let insertRe = await rdb.poolQ(sqlQuery);
    }
  }
};

rdb.checkTableExists = async (tablename) => {
	let checkQuery = `SHOW TABLES LIKE '${tablename}';`;
	let checkJob = await rdb.poolQ(checkQuery);
	console.log(`how many tables ${checkJob.length}`);
	return (checkJob.length>=1||false);
};

rdb.normSchemaToTable = (tableConf) => {
	let plainColumns = [];
	if(tableConf.primaryKey==='autoID') {
		plainColumns.push({name:'id',type:'int',size:11,null:false});
	}
	if(tableConf.foreignKeys||false && tableConf.foreignKeys.length>0||false) {
		for(let f=0,fl=tableConf.foreignKeys.length; f < fl; f++) {
			if(tableConf.foreignKeys[f].refColumn==='id') {
				plainColumns.push({name:tableConf.foreignKeys[f].refName,type:'int',size:11,null:false,comment:`Ref to ${tableConf.foreignKeys[f].table}.${tableConf.foreignKeys[f].column}`});
				continue;
			}
			let refObj = rdb.schema.filter( 
				el => { 
					if(el.tableName === tableConf.foreignKeys[f].table) {
						return el;
					}
				});
			if(refObj[0].fields||false && refObj[0].fields.length>0) {
				let tableField = refObj[0].fields.filter( fel => { 
							if(fel.name === tableConf.foreignKeys[f].refColumn) {
								fel.name = tableConf.foreignKeys[f].refName;
								return fel;
							}
						});
				plainColumns.push(tableField[0]);
			}
		}
	}
	if(tableConf.fields||false && tableConf.fields.length>0) {
		for(let c=0,cl=tableConf.fields.length; c < cl; c++) {
			plainColumns.push(tableConf.fields[c]);
		}
	}

	console.dir(plainColumns);
};

rdb.createTable = async (tableConf) => {
	let creatQuery = 'CREATE TABLE `'+tableConf.tableName+'` (';
	if(tableConf.primaryKey==='autoID') {
		creatQuery += '`id` INTEGER NOT NULL AUTO_INCREMENT,';
		tableConf.primaryKey = 'id';
	}
	for(let f=0,fl=tableConf.fields.length; f < fl; f++) {
		let fConf = tableConf.fields[f];
		creatQuery += '`'+fConf.name+'` ';
		if(fConf.type==='int') {
			creatQuery += 'INTEGER';
		} else if(fConf.type==='varchar') {
			creatQuery += 'VARCHAR';
		}
		creatQuery += `(${fConf.size}) `;
		creatQuery += (fConf["null"]||false)? 'NOT NULL ' : 'NULL ';
		if(fConf.default||false) {
			creatQuery += `DEFAULT '${fConf.default}' `;
		}
		if(fConf.comment||false) {
			creatQuery += `COMMENT '${fConf.comment}' `
		}
		creatQuery += ','
	}
	creatQuery += 'PRIMARY KEY (`'+tableConf.primaryKey+'`)';
	creatQuery += ');';
	
	let howItGoes = await rdb.poolQ(creatQuery);
	console.log(`Create Table => ${tableConf.tableName}\n how it works:`);
	console.dir(howItGoes);
};

rdb.checkTableRows = async (tableConf) => {
	let showColumns = `SHOW COLUMNS FROM ${tableConf.tableName};`;
	// console.dir(await rdb.poolQ(showColumns));
	rdb.normSchemaToTable(tableConf);
};

const initTest = async () => {
	rdb.schema = schema;
	for(let s=0,sl=schema.length; s < sl; s++) {
		// console.dir(schema[s].fields);
		let areYouThere = await rdb.checkTableExists(schema[s].tableName);
		if(areYouThere) {
			await rdb.checkTableRows(schema[s]);
		} else {
			await rdb.createTable(schema[s]);
		}
	}
};


initTest();