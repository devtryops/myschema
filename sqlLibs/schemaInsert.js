const rdb = require('./poolConnect.js');

const intoDB = {};

intoDB.initSchema = (newSchema) => {
	intoDB.schema = newSchema;
	let ttl = {};
	for(let i in intoDB.schema) {
		ttl[intoDB.schema[i].tableName] = i;
	}
	intoDB.tableList = ttl;
};


intoDB.insert = async (inJson) => {
	if(intoDB.tableList[inJson.tableName]||false) {
		let testSch = intoDB.schema[intoDB.tableList[inJson.tableName]];
		let inQuery = 'INSERT INTO `'+inJson.tableName+'` ';
		let inNames = [];
		let inValues = [];
		let testQuery = 'SELECT COUNT(*) AS test FROM `'+inJson.tableName+'` WHERE ';
		let testKeyVal = [];
		for(let t=0,tl=testSch.fields.length; t < tl; t++) {
			let inVal = inJson.fields.filter( e => { return e.name===testSch.fields[t].name });
			let refVal = testSch.fields[t];
			if(inVal.length===1) {
				inNames.push(refVal.name);
				inValues.push(inVal[0].value);
			} else {
				inNames.push(refVal.name);
				inValues.push(refVal.default);
			}
			testKeyVal.push('`'+inNames[inNames.length-1]+"`='"+inValues[inValues.length-1]+"'");
		}
		inQuery += '(`'+inNames.join("`,`")+'`) VALUES ';
		inQuery += `('`+inValues.join("','")+`');`
		testQuery += testKeyVal.join(" AND ")+';';
		// console.log(`My Query:\n${inQuery}\nMyTest:\n${testQuery}`);
		let testReturn = await rdb.poolQ(testQuery);
		console.dir(testReturn[0].test);
		if(testReturn[0].test===0) {
			let doInsert = await rdb.poolQ(inQuery);
			return {done:'Entry done',data:doInsert};
		} else {
			return {info:'Entry already exists'};
		}

	} else {
		return {err:'table schema not found'};
	}

};


module.exports = intoDB;