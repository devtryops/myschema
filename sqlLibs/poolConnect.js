const mysql = require('mysql');

const rdb = {};
const db_config = {};
db_config.host = (process.env.MYSQL_HOST)? process.env.MYSQL_HOST : 'localhost';
db_config.user = (process.env.MYSQL_USER)? process.env.MYSQL_USER : 'root';
db_config.password = (process.env.MYSQL_PASSWORD)? process.env.MYSQL_PASSWORD : 'docker';
db_config.database = (process.env.MYSQL_DATABASE)? process.env.MYSQL_DATABASE : 'schema';
db_config.port = (process.env.MYSQL_PORT)? process.env.MYSQL_PORT : '3206';
db_config.connectionLimit = 10;

rdb.pool = mysql.createPool(db_config);
rdb.poolQ = (sqlQ,args) => {
  return new Promise( ( resolve, reject ) => {
    rdb.pool.getConnection( (err, conn) => {
      if(err) {
        return reject(err);
      } else if(conn) {
        conn.query(sqlQ, args, (err, rows) => {
          conn.destroy();
          if(err) {
            return reject(err);
          } else if(rows){
            return resolve(rows);
          } else {
            return reject({err,rows});
          }
        });
      }
    });
  });
};

module.exports = rdb;